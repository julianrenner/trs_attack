reset()
from sage.coding.relative_finite_field_extension import *
import random, time
load('helper_functions.sage')

# Choose parameters
q0 = 2^8
q1 = q0^2
n = 255
k = 117

l = 1 # implementation works also for l=2
r = ceil( (n+1)/(l+2) ) + 2
t_list = [( (ii+1) + 1)*(r-2) - k + 2 for ii in range(l)]
h_list = [ r - 1 + (ii+1) for ii in range(l)]

print ""
print "Chosen parameters:"
print "q0 = %s" %(q0)
print "q1 = %s" %(q1)

print "n  = %s" %(n)
print "k  = %s" %(k)
print "t  = %s" %(t_list)
print "h  = %s" %(h_list)

## Init fields, maps, arrays
Fq0.<a0>   = GF(q0,modulus='primitive')
Fq1.<a1>  = GF(q1,modulus='primitive')
FE10 = RelativeFiniteFieldExtension(Fq1, Fq0)
phi10 = FE10.embedding()                           # map from Fq0 into Fq1
rho10 = lambda i: FE10.cast_into_relative_field(i) # map from Fq1 into Fq0

##########################################
## Key generation described in Section 3.1
##########################################
# 1. Pick random locators in Fq0
alpha = matrix(Fq0, [a0^ii for ii in random.sample(range(q0-1),n)]).apply_map(phi10)

# 2. Pick random eta that are in Fq\Fq0
eta = [0]*l
for jj in range(l):
    eta[jj] = Fq1.random_element()
    while FE10.is_in_relative_field(eta[jj]):
        eta[jj] = Fq1.random_element()

# 3. Pick random S in Fq
S_mat = matrix.random(Fq1,k,k)
while (S_mat.rank()<k):
    S_mat = matrix.random(Fq1,k,k)

# 4. Compute public key
G_RS = matrix(Fq1,[[ alpha[0,ii]^jj for ii in range(n)] for jj in range(k)])
G_TRS = copy(G_RS)
for ll in range(l):
    G_TRS[h_list[ll],:] = G_TRS[h_list[ll],:] + matrix(Fq1,[eta[ll]*alpha[0,ii]^(k-1+t_list[ll]) for ii in range(n)])
G_pub = S_mat*G_TRS

# Set secrets empty
alpha    = []
eta      = []
S_mat    = []


###################################
## Algorithm 1: Key-Recovery Attack
###################################
print ""
print "Start with key-recovery algorithm."
print "Progress:"

t_start = time.time()
# Line 1: Compute generator matrix of subfield subcode of public code
t_tmp = time.time()
G_sub = SubfieldSubcode(G_pub)
print "Line 1:     completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 2: Compute the square of the subfield subcode
t_tmp = time.time()
G_sq =  Square(G_sub)
print "Line 2:     completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 3: Apply Sidelnikov-Shestakov attack
t_tmp = time.time()
alpha_prime = SidelShest(G_sq)
print "Line 3:     completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 4-10: Transform the code locators, i.e., remove "b"
t_tmp = time.time()
i = 1
while True:
    b = Fq0.list()[i-1] # SageMath starts counting from '0'
    alpha_hat = alpha_prime - matrix(Fq0,[b]*n)
    G_prime = GenSub(alpha_hat)
    i = i+1
    if G_prime*G_sub.right_kernel_matrix().transpose() == matrix.zero(Fq0,k-l,n-(k-l)):
        break

print "Line 4-10:  completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 11-17: Find eta_hat by Interpolation
t_tmp = time.time()
eta_hat = vector(Fq1,l) # declaration of eta_hat
for j in range(l):
    i = 1
    while True:
        g = Interpolate(alpha_hat, G_pub[i-1,:] )
        i = i+1
        if g[h_list[j-1]+1]!=0:
            break
    eta_hat[j-1] = g[k-1+t_list[j-1]] / g[h_list[j-1]]

print "Line 11-17: completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 18: Compute G_hat_TRS matrix
t_tmp = time.time()
G_hat_TRS = GTRS(alpha_hat,eta_hat)
print "Line 18:    completed! Required time: %i seconds." %(time.time() - t_tmp)

# Line 19: Determine S_hat = G_hat_TRS\G_pub
t_tmp = time.time()
S_hat = G_hat_TRS.solve_left(G_pub)
print "Line 19:    completed! Required time: %i seconds." %(time.time() - t_tmp)

## Check if the algorithm was succesful
print ""
if S_hat*G_hat_TRS == G_pub:
    print "--------------------------------------"
    print "Key-Recovery algorithm was successful!"
    print "Total time required: %i seconds." %(time.time() - t_start)
    print "--------------------------------------"
else:
    print "------------------------------------------"
    print "Key-Recovery algorithm was not successful!"
    print "------------------------------------------"
