def SubfieldSubcode(G_mat):
    # Returns a generator matrix of the subfield subcode of the code spaned by the rows of G_mat
    H_mat = G_mat.right_kernel_matrix()
    H_sub = ext_n(Fq1,Fq0,FE10,H_mat)
    G_sub = H_sub.right_kernel_matrix()
    return G_sub

def Square(G_mat):
    # Returns a generator matrix of the squared code spaned by the rows of G_mat
    k = G_mat.nrows()
    n = G_mat.ncols()
    Fq = G_mat.base_ring()
    G_large = matrix(Fq,k^2,n)
    for jj in range(k):
        for kk in range(k):
            row_idx = k*jj+kk
            for ii in range(n):
                G_large[row_idx,ii] = G_mat[jj,ii]*G_mat[kk,ii]
    return G_large.rref()[:G_large.rank(),:]

def SidelShest(G_RS):
    # If G_RS is a generator matrix a Reed_solomon code with locators alpha, the function returns
    # a affine transformation of alpha.

    # This function itself is just a wrapper for the actual Sidelnikov-Shestakov attack. In
    # case 'sidelnikov' outputs a columnmultiplier unequal the all one vectors, permute columns
    # and run it again. Redo the permutation at the end.

    Per_mat = matrix.identity(Fq0,n,n)
    P_mat, x_vec, z_vec = sidelnikov(Fq0,G_RS*matrix.identity(Fq0,n,n))
    while not(z_vec==matrix(Fq0,[1]*n)):
        Per_mat = matrix.identity(Fq0,n,n)[:,sample(range(n),n)]
        P_mat, x_vec, z_vec = sidelnikov(Fq0,G_sq*Per_mat)
    # Redo permutation
    x_vec = x_vec*Per_mat.inverse()
    z_vec = z_vec*Per_mat.inverse()
    return x_vec

def GenSub(x_vec):
    # Returns a matrix for a vector x_vec as defined in Section 4.2
    return matrix(Fq0,[[ x_vec[0,ii]^jj for ii in range(n)] for jj in range(k) if jj not in h_list])

def Interpolate(x_vec, g_pub):
    # Returns a vectors which entries are the coefficients of a polynomial that
    # interpolates x_vec and g_pub
    x_vec = x_vec.apply_map(phi10)
    PR.<x> = Fq1[]
    eva_list = [(x_vec[0,ii],g_pub[0,ii]) for ii in range(n)]
    g = vector(Fq1,PR.lagrange_polynomial(eva_list).coefficients(sparse=false))
    return g

def GTRS(alpha,eta):
    # Returns a generator matrix for a twisted Reed-Solomon code with locators alpha
    # and twist coefficients eta
    x_vec = alpha.apply_map(phi10)
    G_hat = matrix(Fq1,[[ x_vec[0,ii]^jj for ii in range(n)] for jj in range(k)])
    G_TRS_hat = copy(G_hat)
    for jj in range(len(h_list)):
        G_TRS_hat[h_list[jj],:] = G_TRS_hat[h_list[jj],:] + eta[jj]*matrix(Fq1,[x_vec[0,ii]^(k-1+t_list[jj]) for ii in range(n)])

    return G_TRS_hat


# Helper functions for the functions above!
def ext_n(Fl,Fs,Emb,mat):
    # This functions maps the matrix 'mat' with entries in Fl to a matrix with entries in Fs by extending each row of 'mat' to a matrix over Fs and then concatenats the resulting matrices
    ncol = mat.ncols()
    nrow = mat.nrows()
    ext_deg = Emb.extension_degree()

    output = matrix(Fs,ext_deg*nrow,ncol)
    for jj in range(nrow):
        for ii in range(ncol):
            output[jj*ext_deg:(jj+1)*ext_deg,ii] = matrix(Fs,Emb.relative_field_representation(mat[jj,ii])).transpose()
    return output

def SidelShest(G_RS):
    # This function is a wrapper. In case the function sidelnikov has a output with columnmultipliers not equal 1, do random permutation and run it again.
    # At the end redo the permutation

    Per_mat = matrix.identity(Fq0,n,n)
    P_mat, x_vec, z_vec = sidelnikov(Fq0,G_RS*matrix.identity(Fq0,n,n))
    while not(z_vec==matrix(Fq0,[1]*n)):
        Per_mat = matrix.identity(Fq0,n,n)[:,sample(range(n),n)]
        P_mat, x_vec, z_vec = sidelnikov(Fq0,G_sq*Per_mat)

    # Redo permutation
    x_vec = x_vec*Per_mat.inverse()
    z_vec = z_vec*Per_mat.inverse()

    return x_vec


def sidelnikov(Fqm,G_RS):
    # This function is a wrapper. In case G_pub has too high rate, perform sidelnikov algorithm
    # on dual and get the column multiplier correct (s. Wieschebrink: An attack on a modified
    # Niederreiter Ecryption Scheme: Proposition 2)

    if (G_RS.nrows()>G_RS.ncols()/2):
        P_mat, x_vec, u_vec = pure_sidelnikov(Fqm,G_RS.right_kernel_matrix())
        z_vec = matrix(Fqm,1,n)
        for ii in range(n):
            z_vec[0,ii] = u_vec[0,ii]^(-1)
            for jj in range(n):
                if not (jj==ii):
                    z_vec[0,ii] = z_vec[0,ii] * (x_vec[0,ii] - x_vec[0,jj])^(-1)

    else:
        P_mat, x_vec, z_vec = pure_sidelnikov(Fqm,G_RS)

    G_hat_RS = matrix(Fqm,[[x_vec[0,ii]^jj for ii in range(n)] for jj in range(k)])
    Dz_mat = matrix(Fqm,n,n)
    for ii in range(n):
        Dz_mat[ii,ii] = z_vec[0,ii] # siehe Wieschebrink
    G_hat = G_hat_RS*Dz_mat

    return P_mat, x_vec, z_vec

def pure_sidelnikov(Fqm,G_pub):
    # works only for low enough rate. If rate is higher, apply algorithm on dual and get
    # multiplier correct (s. Wieschebrink: An attack on a modified
    # Niederreiter Ecryption Scheme: Proposition 2)
    # This function returns H_mat, x_vec, z_vec
    # where H_mat: rowscrambling matrix, x_vec: evaluation points, z_vec: column multiplier

    k = G_pub.nrows()
    n = G_pub.ncols()

    # 1. Retrieve alternative private key by Sidelnikov-Shestakov
    s = k-1 # since we attack generator matrix (parity-check matrix s = n-k-1
    # Part 1: Recover code locators
    x_vec = matrix(Fqm,1,n)
    x_vec[0,0] = 1
    x_vec[0,1] = 0
    x_vec[0,2] = 0 # should be infinity

    # Step (1) and (3):
    range_10 = range(s+1,2*s)                  # should be: j = s+2,...,2s
    range_20 = range(2,s+1)                    # should be: j = 3,...,s+1

    c1 = G_pub[:,[0]+range_10].left_kernel().basis_matrix()[0,:]
    c2 = G_pub[:,[1]+range_10].left_kernel().basis_matrix()[0,:]
    c3 = G_pub[:,[0]+range_20].left_kernel().basis_matrix()[0,:]
    c4 = G_pub[:,[1]+range_20].left_kernel().basis_matrix()[0,:]

    # Step (2) and (4):
    range_11 = range(3,s+1) + range(2*s,n)   # should be: j = 4,...,s+1,2s+1,...N
    range_21 = range(s+1,2*s)                # should be: j = s+2,...,2s
    beta_mat = block_matrix(4,1,[c1,c2,c3,c4],subdivide=false)*G_pub

    b_vec = matrix(Fqm,1,n)
    b_vec[0,[2]+range_11] = matrix(Fqm,[ beta_mat[0,jj]/beta_mat[1,jj] for jj in [2]+range_11 ])

    b_vec[0,range_21] = matrix(Fqm,[ (beta_mat[3,n-1]/beta_mat[2,n-1])*(beta_mat[2,jj]/beta_mat[3,jj])*b_vec[0,n-1] for jj in range_21 ])

    # Step(5):
    range_x_vec = range(3,n)
    x_vec[0,range_x_vec] = matrix(Fqm,[ b_vec[0,2]/(b_vec[0,2] - b_vec[0,jj]) for jj in range_x_vec])

    # Step (6):
    a_ele = Fqm.multiplicative_generator() # assume a is a generator of the multiplicatiove group
    while (a_ele in x_vec.list()) or (a_ele==0):
        a_ele = a_ele*Fqm.multiplicative_generator()

    x_vec_final = matrix(Fqm,1,n)
    for jj in range(n):
        x_vec_final[0,jj] = 1/(a_ele-x_vec[0,jj])
    x_vec_final[0,2] = 0 # Special since infinity locator

    x_vec = x_vec_final

    ## Part 2: Getting S_mat and Column multiplier
    z_vec = matrix(Fqm,1,n)
    # Step (1):
    c_vec = G_pub[:s+1,:s+2].right_kernel_matrix()[0,:]

    # Step (2):
    cx_mat = matrix(Fqm,[[c_vec[0,jj]*(x_vec[0,jj]^ii) for jj in range(s+2)] for ii in range(s+1)])
    z_vec[0,:s+2] = cx_mat.right_kernel_matrix()
    z_vec = z_vec/z_vec[0,0]

    # Step (3):
    X_mat = matrix(Fqm,[[x_vec[0,ii]^jj for ii in range(s+1)] for jj in range(s+1)])

    Dz = matrix(Fqm,s+1,s+1)
    for ii in range(s+1):
        Dz[ii,ii] = z_vec[0,ii]^(-1)
    G_Dz = G_pub[:,:s+1] *Dz
    H_mat = X_mat.solve_left(G_Dz)

    # Step (4):
    z_vec[0,s+2:] = (H_mat.inverse()[0,:]) * (G_pub[:,s+2:])

    # Last Step: Put things together.
    G_hat_RS = matrix(Fqm,[[x_vec[0,ii]^jj for ii in range(n)] for jj in range(s+1)])
    Dz_mat = matrix(Fqm,n,n)
    for ii in range(n):
        Dz_mat[ii,ii] = z_vec[0,ii]
    G_hat_pub = H_mat*G_hat_RS*Dz_mat

    return H_mat, x_vec, z_vec
